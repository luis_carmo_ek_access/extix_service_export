<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
    return response()->json(array('data' => 'EKTix Export System'), 200);
});

$app->group(['prefix' => 'export'], function ($app) {
    $app->get('order/{order_id}', 'ExportController@exportOrderFiles');
});

$app->post('exported', 'ExportController@exportedFiles');
