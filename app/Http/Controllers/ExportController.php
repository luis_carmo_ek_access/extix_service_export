<?php

namespace App\Http\Controllers;

use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Log;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;

class ExportController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {

        $this->middleware('auth');

    }

    public function exportOrderFiles(Request $request, $order_id)
    {

        if ($this->validateOrder($order_id)) {

            $export_files = $this->getOrderExportFiles($order_id);

            $export_xml = array();

            $export_api = array();

            foreach ($export_files as $export_file) {

                if ($export_file->export_type == 'xml') {

                    if (!array_key_exists($export_file->export_system_id, $export_xml)) {

                        $export_xml[$export_file->export_system_id] = array(
                            'host'      => $export_file->host,
                            'username'  => $export_file->username,
                            'password'  => $export_file->password,
                            'files'     => array()
                        );

                    }

                    array_push($export_xml[$export_file->export_system_id]['files'], $export_file->filename);

                } else if ($export_file->export_type == 'api') {

                    if (!array_key_exists($export_file->export_system_id, $export_api)) {

                        $export_api[$export_file->export_system_id] = array(
                            'host'      => $export_file->host,
                            'username'  => $export_file->username,
                            'password'  => $export_file->password,
                            'verify_certificate' => $export_file->verify_certificate,
                            'files'     => array()
                        );

                    }

                    array_push(
                        $export_api[$export_file->export_system_id]['files'],
                        array(
                            'type'          => $export_file->document_type,
                            'unique_code'   => $export_file->unique_code,
                            'barcode'       => $export_file->barcode,
                            'filename'      => $export_file->filename
                        )
                    );

                } else {

                    return response()->json(array('error' => true, 'message' => 'Export Type Invalid'));

                }

            }

            if (!empty($export_xml)) {

                $this->xmlExport($export_xml);

            }

            if (!empty($export_api)) {

                $this->apiExport($export_api);

            }

        } else {

            return response()->json(array('error' => true, 'message' => 'Order Not Available'));

        }

    }

    private function validateOrder($order_id)
    {

        $available = DB::table('oc_ektix_order')->find($order_id);

        return ($available !== null) ? true : false;

    }

    private function getOrderExportFiles($order_id) {

        $items = DB::table('oc_ektix_export_info AS imp_info')
            ->leftJoin('oc_ektix_product_event_control_access_settings AS ca_settings', 'ca_settings.event_id', '=', 'imp_info.product_event_id')
            ->leftJoin('oc_ektix_export_system AS export_system', 'ca_settings.export_system_id', '=', 'export_system.id')
            ->where('order_id', $order_id)
            ->get(
                [
                    'imp_info.unique_code',
                    'imp_info.barcode',
                    'imp_info.document_type',
                    'imp_info.filename',
                    'export_system.id AS export_system_id',
                    'export_system.host',
                    'export_system.username',
                    'export_system.password',
                    'export_system.verify_certificate',
                    'export_system.type AS export_type'
                ]
            );

        return $items;

    }

    private function saveExportLogs($logs) {

        foreach ($logs as $log) {

            DB::table('oc_ektix_export_logs')->insert(
                [
                    'success' => $log['success'],
                    'message' => $log['message'],
                    'date_created' => DB::raw('now()')
                ]
            );

            if (isset($log['filename'])) {

                DB::table('oc_ektix_export_info')
                    ->where('filename', $log['filename'])
                    ->update(['created' => 1]);

            }

        }

    }

    private function xmlExport($data) {

        $logs = array();

        try {

            foreach ($data as $export) {

                $ftp_server = $export['host'];
                $ftp_user_name = $export['username'];
                $ftp_user_pass = base64_decode($export['password']);

                $conn_id = ftp_connect($ftp_server);

                if (false === $conn_id) {

                    $logs[] = array(
                        'success' => 0,
                        'message' => 'Unable to connect to server: ' . $ftp_server . ''
                    );

                    throw new Exception('Unable to connect to server: ' . $ftp_server);

                }

                $login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);

                if (true === $login_result) {

                    ftp_pasv($conn_id, true);

                    foreach ($export['files'] as $file) {

                        $filename = env('DIR_WL_GESTACCESS') . $file;
                        $destination_file = env('ACCESS_IMP') . $file;

                        $upload = ftp_put($conn_id, $destination_file, $filename, FTP_BINARY);

                        if (!$upload) {

                            $logs[] = array(
                                'success' => 0,
                                'message' => 'FTP upload of ' . $file . ' has failed!'
                            );

                        } else {

                            $logs[] = array(
                                'success' => 1,
                                'message' => 'Uploading ' . $file . ' Completed Successfully!',
                                'filename'=> $file
                            );

                        }

                    }

                } else {

                    $logs[] = array(
                        'success' => 0,
                        'message' => 'Unable to log in: ' . $ftp_user_name . ''
                    );

                    throw new Exception('Unable to log in: ' . $ftp_user_name);

                }

                ftp_close($conn_id);

            }

            $this->saveExportLogs($logs);

            return response()->json(array('success' => true, 'message' => 'Task Completed'));

        } catch (Exception $e) {

            Log::error($e->getMessage());

            return response()->json(array('success' => false, 'message' => 'Task Failed'));

        }


    }

    private function apiExport($data) {

        try {

            foreach ($data as $export) {

                $data_to_send = array(
                    'root' => array(
                        'ServiceAuthenticationHeader' => array(
                            'Username'  => $export['username'],
                            'Password'  => base64_decode($export['password'])
                        ),
                        'TSData' => array(
                            'Header' => array(
                                "Version"   => "EKAccess01",
                                "Issuer"    => "50",
                                "Receiver"  =>  "1"
                            ),
                            'WhitelistRecord' => array(),
                            'BlacklistRecord' => array(),
                        )
                    )
                );

                foreach ($export['files'] as $file) {

                    $file_info = new \stdClass();

                    $file_data = $this->getUniqueCodeData($file);

                    $additional_data = json_decode($file_data->data, true);

                    unset($file_data->data);

                    $ca_code = $this->getProductCA($file_data->product_id);

                    $file_data->ca_code     = $ca_code->code;
                    $file_data->date_end    = $ca_code->date_end;

                    $file_data->gate        = $additional_data['seat_attributes']['porta'][0]['value'];
                    $file_data->zone        = $additional_data['seat_attributes']['zona'][0]['value'];
                    $file_data->block       = $additional_data['seat_attributes']['setor'][0]['value'];
                    $file_data->row         = $additional_data['seat_attributes']['fila'][0]['value'];
                    $file_data->seat        = $additional_data['seat_attributes']['lugar'][0]['value'];

                    $ticket_type = isset($additional_data['tickettype_code_extr']) ? $additional_data['tickettype_code_extr'] : (isset($additional_data['ticket_cod_extr']) ? $additional_data['ticket_cod_extr'] : 0);
                    $person_category = isset($additional_data['ticket_person_category']) ? $additional_data['ticket_person_category'] : '';

                    $file_data->ticket_type = $ticket_type;
                    $file_data->season_pass = 0;
                    $file_data->person_category = $person_category;
                    $file_data->utid2       = 0;

                    if (isset($additional_data['KEYSSOCIO'])) {

                        if (isset($additional_data['KEYSSOCIO']['nr_socio'])) {

                            $file_data->socio = $additional_data['KEYSSOCIO']['nr_socio'];

                        } else {

                            $file_data->socio       = 0;

                        }

                    } else {

                        $file_data->socio       = 0;

                    }

                    if (isset($additional_data['KEYSSOCIO'])) {

                        if (isset($additional_data['KEYSSOCIO']['cliente_id'])) {

                            $file_data->nsocio = $additional_data['KEYSSOCIO']['cliente_id'];

                        } else {

                            $file_data->nsocio       = 0;

                        }

                    } else {

                        $file_data->nsocio       = 0;

                    }

                    $file_info->{'Expire'}  = $file_data->date_end;
                    $file_info->{'UTID'}    = $file_data->barcode;
                    $file_info->{'Action'}  = 'I';
                    $file_info->{'Coding'}  = '0';
                    $file_info->{'Coding'}  = '0';
                    $file_info->{'Permission'} = new \stdClass();

                    // Properties

                    $properties = array();

                    $event = new \stdClass();
                    $event->{'Type'} = 'EVENT';
                    $event->{'ID'} = $ca_code->code;
                    array_push($properties, $event);

                    $area = new \stdClass();
                    $area->{'Type'} = 'AREA';
                    $area->{'ID'} = $file_data->gate;
                    array_push($properties, $area);

                    $tickettype = new \stdClass();
                    $tickettype->{'Type'} = 'TICKETTYPE';
                    $tickettype->{'ID'} = $ticket_type;
                    array_push($properties, $tickettype);

                    $seasonpass = new \stdClass();
                    $seasonpass->{'Type'} = 'SEASONPASS';
                    $seasonpass->{'ID'} = 0;
                    array_push($properties, $seasonpass);

                    $personcategory = new \stdClass();
                    $personcategory->{'Type'} = 'PERSONCATEGORY';
                    $personcategory->{'ID'} = $person_category;
                    array_push($properties, $personcategory);

                    $utid = new \stdClass();
                    $utid->{'Type'} = 'UTID2';
                    $utid->{'ID'} = 0;
                    array_push($properties, $utid);

                    $member = new \stdClass();
                    $member->{'Type'} = 'SOCIO';
                    $member->{'ID'} = $file_data->socio;
                    array_push($properties, $member);

                    $non_member = new \stdClass();
                    $non_member->{'Type'} = 'NSOCIO';
                    $non_member->{'ID'} = $file_data->nsocio;
                    array_push($properties, $non_member);

                    $zone = new \stdClass();
                    $zone->{'Type'} = 'ZONA';
                    $zone->{'ID'} = $file_data->zone;
                    array_push($properties, $zone);

                    $file_info->{'Permission'}->{'TSProperty'} = $properties;

                    if ($file['type'] == 'wl') {

                        array_push($data_to_send['root']['TSData']['WhitelistRecord'], $file_info);

                    } else if ($file['type'] == 'bl') {

                        array_push($data_to_send['root']['TSData']['BlacklistRecord'], $file_info);

                    } else {

                        Log::info('[x] Export File Type Invalid!');

                    }

                }

                try {

                    $client = new Client(['base_uri' => $export['host']]);

                    $headers = [
                        'Content-Type' => 'application/json'
                    ];

                    $response = $client->request(
                        'POST', '', [
                            'headers'   => $headers,
                            'body' => json_encode($data_to_send)
                        ]
                    );

                    if ($response->getStatusCode() === 200) {

                        $response_export = $response->getBody();

                        if (isset($response_export) && !empty($response_export)) {

                            $json_enc = json_decode($response_export->getContents(),true);

                            if (isset($json_enc) && !empty($json_enc)) {

                                if (!empty($json_enc)) {

                                    $this->internalExportedFiles($json_enc);

                                }

                                return response()->json(array('success' => true, 'message' => 'Task Completed'));

                            } else {

                                Log::info('[x] Invalid Response Content!');

                            }

                        } else {

                            Log::info('[x] Invalid Response!');

                        }

                    } else {

                        Log::info('[x] Invalid Request Status!');

                    }

                } catch (RequestException $e) {

                    Log::error($e->getMessage());

                }

                return response()->json(array('success' => true, 'message' => 'Task Completed'));

            }

        } catch (Exception $e) {

            Log::error($e->getMessage());

            return response()->json(array('success' => false, 'message' => 'Task Failed'));

        }

        return true;

    }

    private function getUniqueCodeData($file) {

        $file_data = DB::table('oc_ektix_export_info AS imp_info')
            ->leftJoin('oc_ektix_order_seats_paths AS seat_path', 'seat_path.unique_code', '=', 'imp_info.unique_code')
            ->leftJoin('oc_ektix_seat_sold AS seat_sold', 'seat_sold.seat_id', '=', 'seat_path.seat_id')
            ->where('imp_info.unique_code', $file['unique_code'])
            ->get(
                [
                    'seat_path.order_id',
                    'seat_path.product_id',
                    'seat_path.seat_id',
                    'seat_path.barcode',
                    'seat_path.unique_code',
                    'seat_path.barcode',
                    'seat_sold.ticket_id',
                    'seat_path.data'
                ]
            )->first();

        return $file_data;

    }

    private function getProductCA($product_id) {

        $code = DB::table('oc_ektix_product_event_control_access_settings')
            ->where('event_id', $product_id)
            ->get(
                [
                    'control_access_code AS code',
                    'close_doors_at AS date_end'
                ]
            )->first();

        return $code;

    }

    public function exportedFiles(Request $request) {

        try {

            if (!$request->isJson()) {
                return response()->json(array('error' => 'Content-Type Not Allowed'), 406);
            }

            $this->validate($request, [
                'exported' => 'required|array'
            ]);

            $exported = $request->json()->get('exported');

            if (count($exported) > 50) {

                return response()->json(array('error' => true, 'message' => 'Number of Records Exceeded.'), 200);

            } else {

                foreach ($exported as $export) {

                    DB::table('oc_ektix_export_info')
                        ->where('filename', $export)
                        ->update(['exported' => 1]);

                }

                return response()->json(array('success' => true, 'message' => 'Successfully Processed'), 200);

            }

        } catch (\Mockery\Exception $e) {

            return response()->json(array('error' => true, 'message' => 'Internal Error'), 500);

        }

    }

    public function internalExportedFiles($files) {

        foreach ($files as $file) {

            DB::table('oc_ektix_export_info')
            ->where('barcode', '=', $file['utid'])
            ->where('document_type', '=', strtolower($file['document_type']))
            ->update([
                'created'       => 1,
                'exported'      => 1,
                'export_type'   => 'api'
            ]);

        }

    }

}
