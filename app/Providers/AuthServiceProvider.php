<?php

namespace App\Providers;

use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {
        // Here you may define how you wish users to be authenticated for your Lumen
        // application. The callback which receives the incoming request instance
        // should return either a User instance or null. You're free to obtain
        // the User instance via an API token or any other method necessary.

        /*
        $this->app['auth']->viaRequest('api', function ($request) {
            if ($request->input('api_token')) {
                return User::where('api_token', $request->input('api_token'))->first();
            }
        });
        */

        $this->app['auth']->viaRequest('api', function ($request) {
            if ($request->header('x-api-key') && $request->getUser() && $request->getPassword()) {

                return $this->login($request->header('x-api-key'), $request->getUser(), $request->getPassword());

            } else {

                return $this->loginIntegration($request->getUser(), $request->getPassword());

            }
        });

    }

    private function login($api_key, $username, $password)
    {

        $user_pos = DB::table('oc_ektix_api as a')
            ->join('oc_ektix_user_ekpos_api as uea', 'a.id', '=', 'uea.api_id')
            ->join('oc_ekpos_user as u', 'uea.user_id', '=', 'u.user_id')
            ->where([
                ['a.key', '=', $api_key],
                ['a.status', '=', '1'],
                ['u.status', '=', '1'],
                ['u.username', '=', $username]
            ])->Where(function ($query) use ($password) {
                $query->orWhere('u.password', DB::raw("SHA1(CONCAT(salt, SHA1(CONCAT(salt, SHA1('".$password."')))))"))
                    ->orWhere('u.password', '=', $password);
            })->select('u.user_id as id')->first();

        return isset($user_pos->id) ? $user_pos->id : null;

    }

    private function loginIntegration($username, $password) {

        $valid_integration = DB::table('oc_ektix_integrations as integration')
            ->where([
                ['integration.username', '=', $username]
            ])->Where(function ($query) use ($password) {
                $query->orWhere('integration.password', DB::raw("SHA1(CONCAT(salt, SHA1(CONCAT(salt, SHA1('".$password."')))))"))
                    ->orWhere('integration.password', '=', $password);
            })->select('integration.id as id')->first();

        return isset($valid_integration->id) ? $valid_integration->id : null;

    }

}
